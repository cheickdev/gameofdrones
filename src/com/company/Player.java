package com.company;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    static class Zone {
        int X;
        int Y;
        int P;
        int D;
        int[] player;
        Zone(int X, int Y) {
            this.X = X;
            this.Y = Y;
        }

        @Override
        public String toString() {
            return X + " " + Y;
        }
    }

    static class Drone {
        int X;
        int Y;
        int ID;
        Zone mZone;
        Drone(int X, int Y) {
            this.X = X;
            this.Y = Y;
        }

        double distance(Zone zone) {
            if (zone == null) {
                return Double.MAX_VALUE;
            }
            return Math.hypot(zone.X - X, zone.Y - Y);
        }

        void move(Zone[] zones, int[] controller) {
            main: for (int i = 0; i < zones.length; i++) {
                if(controller[i] == -1) {
                    if (mZone == null) {
                        mZone = zones[i];
                    }
                    if (zones[i].player[ID] > 0) {
                        mZone = zones[i];
                        break main;
                    } else {
                        for (int j = 0; j < zones[i].P; j++) {
                            if (zones[i].player[j] == 1) {
                                mZone = zones[i];
                                break main;
                            }
                        }
                        continue main;
                    }
                } else {
                    if (controller[i] != ID) {
                        if(zones[i].player[controller[i]] - zones[i].player[ID] == 0) {
                            mZone = zones[i];
                            break main;
                        }
                    } else {
                        for (int j = 0; j < zones[i].P; j++) {
                            if (j != ID) {
                                if (zones[i].player[ID] - zones[i].player[j] == 1) {
                                    continue main;
                                }
                            }
                        }
                        continue main;
                    }
                }
            }
            System.out.println(mZone);
        }
    }

    static void compute(Zone[] zones, Drone[] drones, int[] controller, int ID) {
        for (int i = 0; i < zones.length; i++) {
            Zone zone = zones[i];
            for (int j = 0; j < zone.D; j++) {
                Drone mDrone = drones[ID * zone.D + j];
                if(mDrone.distance(zone) <= 100) {
                    mDrone.mZone = zone;
                    zone.player[ID]++;
                }
                if(controller[i] != -1) {
                    Drone cDrone = drones[controller[i] * zone.D + j];
                    if(cDrone.distance(zone) <= 100) {
                        cDrone.mZone = zone;
                        zone.player[controller[i]]++;
                    }
                } else {
                    for (int k = 0; k < zone.P; k++) {
                        Drone cDrone = drones[k * zone.D + j];
                        if(cDrone.distance(zone) <= 100) {
                            cDrone.mZone = zone;
                            zone.player[k]++;
                        }
                    }
                }
            }
        }
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int P = in.nextInt(); // number of players in the game (2 to 4 players)
        int ID = in.nextInt(); // ID of your player (0, 1, 2, or 3)
        int D = in.nextInt(); // number of drones in each team (3 to 11)
        int Z = in.nextInt(); // number of zones on the map (4 to 8)

        Zone[] zones = new Zone[Z];

        for (int i = 0; i < Z; i++) {
            int X = in.nextInt(); // corresponds to the position of the center of a zone. A zone is a circle with a radius of 100 units.
            int Y = in.nextInt();
            zones[i] = new Zone(X, Y);
            zones[i].P = P;
            zones[i].D = D;
            zones[i].player = new int[P];
        }

        // game loop
        while (true) {
            int controller[] = new int[Z];
            for (int i = 0; i < Z; i++) {
                int TID = in.nextInt(); // ID of the team controlling the zone (0, 1, 2, or 3) or -1 if it is not controlled. The zones are given in the same order as in the initialization.
                controller[i] = TID;
            }
            Drone[] drones = new Drone[P * D];
            for (int i = 0; i < P; i++) {
                for (int j = 0; j < D; j++) {
                    int DX = in.nextInt(); // The first D lines contain the coordinates of drones of a player with the ID 0, the following D lines those of the drones of player 1, and thus it continues until the last player.
                    int DY = in.nextInt();
                    drones[i * D + j] = new Drone(DX, DY);
                    drones[i * D + j].ID = i;
                }
            }

            compute(zones, drones, controller, ID);

            for (int i = 0; i < D; i++) {

                // Write an action using System.out.println()
                // To debug: System.err.println("Debug messages...");
                drones[ID * D + i].move(zones, controller);

                //System.out.println("20 20"); // output a destination point to be reached by one of your drones. The first line corresponds to the first of your drones that you were provided as input, the next to the second, etc.
            }
        }
    }
}