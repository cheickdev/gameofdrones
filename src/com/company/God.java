package com.company;

import java.util.*;

/**
 * Created by sissoko on 07/04/2016.
 */
public class God {

    static class Vect implements Comparable<Vect> {
        int x;
        int y;

        Vect(int x, int y) {
            this.x = x;
            this.y = y;
        }

        double norm() {
            return Math.hypot(x, y);
        }

        int turns2(Vect target) {
            return (int) (Math.ceil(moins(target).norm() / 100) + 0.1);
        }

        int turns2(Zone zone) {
            return turns2(zone.pos);
        }

        double dot(Vect v) {
            return x * v.x + y * v.y;
        }

        double det(Vect v) {
            return x * v.y - y * v.x;
        }

        Vect moins(Vect v) {
            return new Vect(x - v.x, y - v.y);
        }

        Vect plus(Vect v) {
            return new Vect(x + v.x, y + v.y);
        }

        static Vect div(Vect v, double r) {
            return new Vect((int) (v.x / r), (int) (v.y / r));
        }

        static Vect time(Vect v, double r) {
            return new Vect((int) (r * v.x), (int) (r * v.y));
        }

        static Vect time(double r, Vect v) {
            return new Vect((int) (r * v.x), (int) (r * v.y));
        }

        @Override
        public int compareTo(Vect o) {
            if (x < o.x) {
                return 1;
            }
            if (x > o.x) {
                return -1;
            }
            if (y < o.y) {
                return 1;
            }
            if (y > o.y) {
                return -1;
            }
            return 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Vect vect = (Vect) o;

            if (x != vect.x) return false;
            return y == vect.y;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        @Override
        public String toString() {
            return x + " " + y;
        }
    }

    static class Zone {
        Vect pos;
        int id;
        int owner_id;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Drone drone = (Drone) o;

            return pos != null ? pos.equals(drone.pos) : drone.pos == null;

        }

        @Override
        public int hashCode() {
            return pos != null ? pos.hashCode() : 0;
        }
    }

    static class Drone {
        int id;
        int player_id;
        Vect pos;
        Vect speed = new Vect(0, 0);
        Zone expected_dest;
        double turns2dest;

        Drone(int x, int y) {
            this.pos = new Vect(x, y);
        }

        int turns2(Vect target) {
            return pos.turns2(target);
        }

        int turns2(Zone zone) {
            return turns2(zone.pos);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Drone drone = (Drone) o;

            if (id != drone.id) return false;
            return player_id == drone.player_id;

        }

        @Override
        public int hashCode() {
            int result = id;
            result = 31 * result + player_id;
            return result;
        }
    }

    static class Gamer {
        Vector<Zone> zones;
        Drone[] drones;
        Vect zone_center;
        int score = 0;
        int id;

        Gamer() {
            zones = new Vector<>();
            zone_center = new Vect(0, 0);
        }
    }

    static class Task {
        int drone_id;
        Vect pos;

        public Task(int drone_id, Vect pos) {
            this.drone_id = drone_id;
            this.pos = pos;
        }
    }

    static class Objective {
        Zone zone = null;
        int radius;
        int n_needed;
        Vector<Integer> candidates; //drone id
        int value;
        Vector<Objective> depends;
        boolean done = false;
        int n_contractors = 0;

        Objective() {
            candidates = new Vector<>();
            depends = new Vector<>();
        }

        Objective copy() {
            Objective objective = new Objective();
            objective.zone = zone;
            objective.radius = radius;
            objective.n_needed = n_needed;
            objective.candidates = candidates;
            objective.value = value;
            objective.depends = depends;
            objective.done = done;
            objective.n_contractors = n_contractors;
            return objective;
        }
    }

    static class DecisionContext {
        Vector<Task> task_per_drone;
        Vector<Set<Vect>> inter_set;
        Vector<Vector<Objective>> contracts;

        DecisionContext() {
            task_per_drone = new Vector<>(n_drones);
            inter_set = new Vector<>(n_drones);
            contracts = new Vector<>(n_drones);
            for (int i = 0; i < n_drones; i++) {
                contracts.add(new Vector<>());
                inter_set.add(new HashSet<>());
                task_per_drone.add(new Task(0, new Vect(0, 0)));
            }
        }

        boolean add_Objective(Objective obj) {
            if (obj.done)
                return true;
            Vector<Task> tmp_tasks = new Vector<>();
            Vector<Set<Vect>> tmp_inter_set = new Vector<>(n_drones);
            for (int i = 0; i < n_drones; i++) {
                tmp_inter_set.add(i, new HashSet<>());
            }
            Vector<Vector<Objective>> saved_contracts = new Vector<>(n_drones);
            for (int i = 0; i < n_drones; i++) {
                saved_contracts.add(i, new Vector<>());
            }
            for (int di : obj.candidates) {
                Drone d = players.get(my_id).drones[di];
                if (!contracts.get(di).isEmpty()) {
                    //std::cerr<<"drone "<<di<<" contracted to zones: ";
                    //for(auto c:contracts[di])std::cerr<<c->zone->id()<<"("<<c->radius<<") "; std::cerr<<std::endl;
                    //Set<Vect> inter_others = inter_set.get(di);
                    ArrayList<Vect> inter_others = new ArrayList<>(inter_set.get(di));
                    for (int i = 0; i < inter_others.size(); i++) {
                        Vect v = inter_others.get(i);
                        if (v.turns2(obj.zone) > obj.radius) {
                            inter_others.remove(v);
                        }
                    }

                    //not compatible (else it is)
                    if (!inter_others.isEmpty()) {
                        tmp_inter_set.set(di, new HashSet<>(inter_others));
                        Set<Vect> hs = new HashSet<>();
                        intersect(d.pos, 1, obj.zone.pos, obj.radius + 1,
                                hs);
                        for (Objective c : contracts.get(di))
                            intersect(c.zone.pos, c.radius + 1, obj.zone.pos,
                                    obj.radius + 1, hs);
                        ArrayList<Vect> inter_new = new ArrayList<>(hs);
                        for (int i = 0; i < inter_new.size(); i++) {
                            Vect v = inter_new.get(i);
                            if (v.moins(d.pos).norm() > 100)
                                inter_new.remove(v);
                        }
                        for (Objective c : contracts.get(di))
                            for (int i = 0; i < inter_new.size(); i++) {
                                Vect v = inter_new.get(i);
                                if (v.turns2(c.zone) > c.radius)
                                    inter_new.remove(v);
                            }
                        tmp_inter_set.get(di).addAll(inter_new);

                        Vect m = new Vect(0, 0);
                        for (Vect i : tmp_inter_set.get(di))
                            m = m.plus(i);
                        m = Vect.div(m, tmp_inter_set.get(di).size());
                        //check for rounding errors
                        boolean all_ok = true;
                        if (d.turns2(m) > 1) {
                            System.err.printf("Merging contracts failed when adding (%d, dist " +
                                    "%d) (point too far)\n", obj.zone.id, obj.radius);
                            for (Vect i : tmp_inter_set.get(di))
                                System.err.println(i + " : " + d.turns2(i));
                            all_ok = false;
                        }
                        for (Objective c : contracts.get(di))
                            if (m.turns2(c.zone) > c.radius) {
                                System.err.printf("Merging contracts failed when adding (%d, dist " +
                                        "%d) (zone %d, dist %d)\n", obj.zone.id, obj.radius, c.zone.id, c.radius);

                                for (Vect i : tmp_inter_set.get(di))
                                    System.err.println(i + " : " + i.turns2(c.zone));
                                all_ok = false;
                            }
                        if (all_ok) {
                            tmp_tasks.add(new Task(di, m));
                            continue;
                        }
                    } else {
                        if (tmp_tasks.size() >= obj.n_needed)
                            continue;
                        boolean available = true;
                        for (Objective c : contracts.get(di))
                            if (c.n_contractors == c.n_needed) {
                                available = false;
                                break;
                            }
                        if (!available)
                            continue;
                        for (Objective c : contracts.get(di))
                            c.n_contractors--;
                        saved_contracts.set(di, contracts.get(di));
                        contracts.get(di).clear();
                        tmp_inter_set.get(di).clear();
                    }
                }
                if (contracts.get(di).isEmpty()) {
                    intersect(d.pos, 1, obj.zone.pos, obj.radius + 1,
                            tmp_inter_set.get(di));
                    if (tmp_inter_set.get(di).isEmpty())
                        for (float i = 0; i < 6; i += 1)
                            tmp_inter_set.get(di).add(
                                    d.pos.plus(new Vect(
                                            (int) (100 * Math.cos(2 * Math.PI * i / 6)), (int) (100 * Math.sin(2 * Math.PI * i / 6)))));
                    tmp_tasks.add(new Task(di, obj.zone.pos));
                }
                //if(tmp_tasks.size()>=obj.n_needed)break;
            }
            if (tmp_tasks.size() >= obj.n_needed) {
                for (Task t : tmp_tasks) {
                    task_per_drone.set(t.drone_id, t);
                    inter_set.set(t.drone_id, tmp_inter_set.get(t.drone_id));
                    contracts.get(t.drone_id).add(obj);
                    System.err.printf("assigned drone %d to (%d, dist %d)\n", t.drone_id, obj.zone.id, obj.radius);
                }
                obj.n_contractors = tmp_tasks.size();
                return true;
            }
            for (int di = 0; di < n_drones; di++)
                for (Objective c : saved_contracts.get(di)) {
                    contracts.get(di).add(c);
                    c.n_contractors++;
                }
            return false;
        }

        DecisionContext copy() {
            DecisionContext context = new DecisionContext();
            context.task_per_drone = (Vector<Task>) task_per_drone.clone();
            context.inter_set = (Vector<Set<Vect>>) inter_set.clone();
            context.contracts = (Vector<Vector<Objective>>) contracts.clone();
            return context;
        }
    }

    static int flip_coin() {
        return random_engine.nextDouble() < 0.5 ? 1 : 0;
    }

    static void intersect(Vect c1, double r1_turns, Vect c2, double r2_turns,
                          Set<Vect> inter) {
        Vect c2toc1 = c1.moins(c2);
        double r1 = (r1_turns) * 100 - 1, r2 = (r2_turns) * 100 - 1,
                dist = c2toc1.norm();
        if (dist == 0) {
            return;
        }    //careful...
        if (dist > r1 + r2) {
            return;
        }    //disks are disjoint
        if (dist + r2 < r1) {
            return;
        } //c2 is inside c1
        if (dist + r1 < r2) {
            return;
        } //c1 is inside c2
        double frac = ((r2 * r2 - r1 * r1) / dist / dist + 1) / 2;
        double h = Math.sqrt(r2 * r2 - frac * dist * frac * dist);
        Vect m = c2.plus(Vect.time(c2toc1, frac));
        Vect inter1 = m.plus(Vect.time(h, Vect.div(new Vect(c2toc1.y, -c2toc1.x), dist)));
        Vect inter2 = m.plus(Vect.time(h, Vect.div(new Vect(-c2toc1.y, c2toc1.x), dist)));
        if (inter1.x > 0 && inter1.y > 0 && inter1.x < map_size.x
                && inter1.y < map_size.y)
            inter.add(inter1);
        if (inter2.x > 0 && inter2.y > 0 && inter2.x < map_size.x
                && inter2.y < map_size.y)
            inter.add(inter2);
        //std::cerr<<c1<<","<<r1<<" inter "<<c2<<","<<r2<<" : "<<(m+h*Vect({c2toc1.y,-c2toc1.x})/dist)<<", "<<(m+h*Vect({-c2toc1.y,c2toc1.x})/dist)<<std::endl;
    }


    static final Vect map_size = new Vect(4000, 1800);
    static int n_zones, n_drones, n_players, my_id;
    static Vector<Gamer> players;
    static Zone[] zones;

    static Random random_engine = new Random();

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        n_players = in.nextInt(); // number of players in the game (2 to 4 players)
        my_id = in.nextInt(); // ID of your player (0, 1, 2, or 3)
        n_drones = in.nextInt(); // number of drones in each team (3 to 11)
        n_zones = in.nextInt(); // number of zones on the map (4 to 8)

        random_engine.setSeed(my_id * 27);
        players = new Vector<>(n_players);
        for (int i = 0; i < n_players; i++) {
            Gamer gamer = new Gamer();
            gamer.id = i;
            gamer.drones = new Drone[n_drones];
            for (int j = 0; j < n_drones; j++) {
                gamer.drones[j] = new Drone(-1, -1);
            }
            players.add(gamer);
        }

        zones = new Zone[n_zones];
        for (int i = 0; i < n_zones; i++) {
            int X = in.nextInt(); // corresponds to the position of the center of a zone. A zone is a circle with a radius of 100 units.
            int Y = in.nextInt();
            Zone zone = new Zone();
            zone.pos = new Vect(X, Y);
            zone.id = i;
            zones[i] = zone;
        }

        Zone[] sorted_zones = Arrays.copyOf(zones, zones.length);
        Arrays.sort(sorted_zones, new Comparator<Zone>() {
            @Override
            public int compare(Zone a, Zone b) {
                return a.pos.x < b.pos.x ? 1 : 0;
            }
        });
        //int best = 0;

        Vect lhome = Vect.div(sorted_zones[0].pos.plus(sorted_zones[1].pos).plus(sorted_zones[2].pos), 3);
        Vect rhome = Vect.div(sorted_zones[n_zones - 1].pos.plus(sorted_zones[n_zones - 2].pos).plus(sorted_zones[n_zones - 3].pos), 3);
        Vect lfront = sorted_zones[3].pos;
        Vect rfront = new Vect(0, 0);
        if (n_zones > 4) {
            lfront = Vect.div(lfront.plus(sorted_zones[4].pos), 2);
            rfront = sorted_zones[n_zones - 4].pos;
        }
        if (n_zones > 5) {
            rfront = Vect.div(rfront.plus(sorted_zones[n_zones - 5].pos), 2);
        }
        Vect best3_home =
                lhome.moins(lfront).norm() < rhome.moins(rfront).norm() ? rhome : lhome;
        //Vect* default_home=(n_players==2)?&players[!my_id).zone_center:new Vect(map_size/2);
        Vect default_home =
                (n_players == 2) ? players.get(1 - my_id).zone_center :
                        (n_players == 3) ? Vect.div(map_size, 2) : best3_home;
        Vect curr_home = default_home;
        //compact, far from the rest, far from enemies is better

        // game loop
        while (true) {
            for (Gamer p : players)
                p.zones.clear();
            for (int i = 0; i < n_zones; i++) {
                int TID = in.nextInt(); // ID of the team controlling the zone (0, 1, 2, or 3) or -1 if it is not controlled. The zones are given in the same order as in the initialization.
                Zone z = zones[i];
                zones[i].owner_id = TID;
                if (z.owner_id != -1) {
                    players.get(z.owner_id).zones.add(z);
                    players.get(z.owner_id).score++;
                }
            }
            for (Gamer player : players) {
                for (int i = 0; i < n_drones; i++) {
                    int DX = in.nextInt(); // The first D lines contain the coordinates of drones of a player with the ID 0, the following D lines those of the drones of player 1, and thus it continues until the last player.
                    int DY = in.nextInt();
                    Drone d = player.drones[i];
                    Vect prev_pos = player.drones[i].pos;
                    d.id = i;
                    d.player_id = player.id;
                    d.pos.x = DX;
                    d.pos.y = DY;
                    if (prev_pos.x < 0) {
                        d.id = i;
                        continue;
                    }
                    d.speed = d.pos.moins(prev_pos);
                    d.expected_dest = null;
                    for (int j = 0; j < zones.length; j++) {
                        if (d.speed.norm() > 70
                                && Math.abs(
                                zones[j].pos.moins(d.pos).det(
                                        Vect.div(d.speed, d.speed.norm()))) < 100
                                && zones[j].pos.moins(d.pos).dot(d.speed) > 0) {
                            int dist = d.turns2(zones[j]);
                            if (d.expected_dest != null && d.turns2dest < dist)
                                continue;
                            if (d.expected_dest != null)
                                ;    //expected_dest changed !
                            d.expected_dest = zones[j];
                            d.turns2dest = dist;
                        }
                    }
                }
                if (player.zones.isEmpty()) {
                    player.zone_center = Vect.div(map_size, 2);
                } else {
                    player.zone_center = new Vect(0, 0);
                    for (Zone z : player.zones) {
                        player.zone_center = player.zone_center.plus(z.pos);
                    }
                    player.zone_center = Vect.div(player.zone_center, player.zones.size());

                }
            }
            curr_home =
                    (players.get(my_id).zones.size() >= 3) ?
                            players.get(my_id).zone_center : default_home;

            //find objectives
            LinkedList<Objective> objectives = new LinkedList<>();
            for (Zone z : zones) {
                boolean is_my_zone = (z.owner_id == my_id);
                //compute attackers for zone z
                for (Gamer p : players) {
                    Arrays.sort(p.drones, new Comparator<Drone>() {
                        @Override
                        public int compare(Drone a, Drone b) {
                            if (a.turns2(z) < b.turns2(z)) {
                                return 1;
                            }
                            if (a.turns2(z) > b.turns2(z)) {
                                return -1;
                            }
                            return 0;
                        }
                    });
                    System.err.println(p.drones[0].pos);
                }
                Vector<Drone>[] attackers = new Vector[n_drones];
                for (int i = 0; i < n_drones; i++) {
                    attackers[i] = new Vector<>();
                }
                for (int di = 0; di < n_drones; di++) {
                    attackers[di].add(
                            players.get((my_id + 1) % n_players).drones[di]);
                }
                for (int pi = 2; pi < n_players; pi++) {
                    for (int di = 0; di < n_drones; di++) {
                        int diff =
                                attackers[di].get(0).turns2(z)
                                        - players.get((my_id + pi) % n_players).drones[di].turns2(z);
                        if (diff > 0)
                            attackers[di].clear();
                        if (diff >= 0)
                            attackers[di].add(
                                    players.get((my_id + pi) % n_players).drones[di]);
                    }
                }
                System.err.printf((is_my_zone ? "+" : " ") + "Zone %d", z.id);
                System.err.print(" allies :");
                for (Drone d : players.get(my_id).drones)
                    System.err.print(d.turns2(z) + " ");
                System.err.print(" enemies : ");
                for (Vector<Drone> a : attackers)
                    System.err.println(a.get(0).turns2(z) + " ");
                System.err.print(" contested :");

                Vector<Objective> curr_depends = new Vector<>();
                //compute objectives for zone z
                for (int di = 0; di < n_drones - 1; di++) {
                    boolean contested = false;
                    for (Drone d : attackers[di])
                        if (z.equals(d.expected_dest) || d.turns2(z) == 0) {
                            contested = true;
                            break;
                        }
                    System.err.println((contested ? "x " : "o "));
                    if (is_my_zone) {
                        if (attackers[di].get(0).turns2(z)
                                == attackers[di + 1].get(0).turns2(z))
                            continue;
                        if(attackers[di + 1].get(0).turns2(z) == 0) {
                            System.err.println("attackers[di + 1].get(0).turns2(z) " + attackers[di + 1].get(0));
                        }
                        int mydi = di;
                        int diff = attackers[di].get(0).turns2(z)
                                - players.get(my_id).drones[mydi].turns2(z);
                        Objective obj = new Objective();
                        obj.radius = Math.max(attackers[di].get(0).turns2(z) - 1, 0);
                        if (diff < 0) {
                            is_my_zone = false;
                            continue;
                        }    //cannot defend
                        else if (diff <= 1) {
                            while (mydi + 1 < n_drones
                                    && players.get(my_id).drones[mydi + 1].turns2(z)
                                    <= obj.radius + 1)
                                mydi++;
                            for (int i = 0; i <= mydi; i++)
                                if (players.get(my_id).drones[i].turns2(z)
                                        >= obj.radius)
                                    obj.candidates.add(
                                            players.get(my_id).drones[i].id);
                        } else if (diff > 1)
                            continue;    //no action needed

                        obj.zone = z;
                        obj.value = 42 + 10 * (attackers[di + 1].get(0).turns2(z)
                                - attackers[di].get(0).turns2(z)) / attackers[di + 1].get(0).turns2(z);

                        obj.n_needed = di + 1 - (mydi + 1 - obj.candidates.size());
                        obj.depends = curr_depends;
                        objectives.add(obj);
                        curr_depends.add(objectives.getLast());
                    } else
                        for (int mydi = di; mydi <= di; mydi++) {
                            //for(int mydi=di;mydi<n_drones;mydi++){
                            while (mydi + 1 < n_drones
                                    && players.get(my_id).drones[mydi].turns2(z)
                                    == players.get(my_id).drones[mydi + 1].turns2(
                                    z))
                                mydi++;
                            int diff = attackers[di].get(0).turns2(z)
                                    - players.get(my_id).drones[mydi].turns2(z);
                            Objective obj = new Objective();
                            obj.radius = Math.max(
                                    players.get(my_id).drones[mydi].turns2(z) - 1, 0);
                            obj.zone = z;
                            if (diff >= 1) {
                                is_my_zone = true;
                                for (int i = 0; i <= mydi; i++)
                                    if (players.get(my_id).drones[i].turns2(z)
                                            >= obj.radius)
                                        obj.candidates.add(
                                                players.get(my_id).drones[i].id);
                                obj.value = 40 + 10 *
                                        (attackers[di + 1].get(0).turns2(z) - players.get(my_id).drones[mydi].turns2(z)) / attackers[di + 1].get(0).turns2(z);
                            } else if (!contested) {
                                if (diff >= -1) {
                                    for (int i = 0; i <= mydi; i++)
                                        if (players.get(my_id).drones[i].turns2(z)
                                                >= obj.radius)
                                            obj.candidates.add(
                                                    players.get(my_id).drones[i].id);
                                    obj.value = 10 + 10 * (attackers[di + 1].get(0).turns2(z) - players.get(my_id).drones[mydi].turns2(z)) / attackers[di + 1].get(0).turns2(z);
                                } else {
                                    continue;
                                }
                            } else
                                continue;
                            obj.n_needed = di + 1
                                    - (mydi + 1 - obj.candidates.size());
                            objectives.add(obj);
                            if (is_my_zone) {
                                curr_depends.clear();
                                curr_depends.add(objectives.getLast());
                            }
                        }
                }
                System.err.println(z.pos);
            }
            System.err.println("Found " + objectives.size() + " objectives.");

            //chose a set of objectives and assign tasks
            DecisionContext decisions = new DecisionContext();

            for (Gamer p : players)
                Arrays.sort(p.drones, new Comparator<Drone>() {
                    @Override
                    public int compare(Drone a, Drone b) {
                        if (a.id < b.id) {
                            return 1;
                        }
                        if (a.id > b.id) {
                            return -1;
                        }
                        return 0;
                    }
                });
            //for(auto& obj:objectives)obj.value=obj.value/std::sqrt(obj.radius+1);
            Collections.sort(objectives, new Comparator<Objective>() {
                @Override
                public int compare(Objective a, Objective b) {
                    if (a.value == b.value) {
                        return flip_coin();
                    }
                    if (a.value > b.value) {
                        return 1;
                    }
                    if (a.value < b.value) {
                        return -1;
                    }
                    return 0;
                }
            });

            for (Objective obj : objectives) {
                obj.depends.add(obj);
                boolean dep_done = true;
                DecisionContext tmp_context = decisions.copy();
                for (Objective dep : obj.depends)
                    if (!tmp_context.add_Objective(dep)) {
                        dep_done = false;
                        break;
                    }
                if (dep_done) {
                    for (Objective dep : obj.depends)
                        dep.done = true;
                    decisions = tmp_context;
                } else {                    //this is not nice but...
                    for (Objective o : objectives)
                        o.n_contractors = 0;
                    for (int di = 0; di < n_drones; di++)
                        for (Objective c : decisions.contracts.get(di)) {
                            c.n_contractors++;
                        }
                }
            }
            for (int di = 0; di < n_drones; di++) {
                boolean available = true;
                for (Objective c : decisions.contracts.get(di))
                    if (c.n_contractors == c.n_needed) {
                        available = false;
                        break;
                    }
                if (!available)
                    continue;
                for (Objective c : decisions.contracts.get(di))
                    c.n_contractors--;
                decisions.contracts.get(di).clear();
            }
            for (Objective obj : objectives) {
                System.err.println((obj.done ? "!" : " ") + "score: " + obj.value
                        + ", " + obj.n_needed + " drone(s) needed to ");
                if (obj.zone != null)
                    System.err.printf("zone %d", obj.zone.id);
                else
                    System.err.print("home?");
                System.err.print(" at dist " + obj.radius + " from:");
                for (Integer di : obj.candidates) {
                    boolean contracted = false;
                    for (Objective c : decisions.contracts.get(di))
                        if (c.equals(obj)) {
                            contracted = true;
                            break;
                        }
                    System.err.print(" " + (contracted ? "!" : " ") + di);
                }
                System.err.println();
            }
            System.err.println("--------" + curr_home + "--------");
            for (int i = 0; i < n_drones; i++)
                if (decisions.contracts.get(i).isEmpty()) {
                    decisions.task_per_drone.set(i, new Task(i, curr_home));
                }

            //output results
            System.err.println("decisions.task_per_drone = " + decisions.task_per_drone.size());
            for (Task t : decisions.task_per_drone) {
                if (t.pos != null) {
                    System.out.println(t.pos);
                } else {
                    System.err.println("What's wrong.....");
                }
            }
        }
    }
}
