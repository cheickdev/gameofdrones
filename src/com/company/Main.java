package com.company;

import java.util.Scanner;

public class Main {

    static class Zone {
        int X;
        int Y;

        Zone(int X, int Y) {
            this.X = X;
            this.Y = Y;
        }

        @Override
        public String toString() {
            return X + " " + Y;
        }
    }

    static class Drone {
        int X;
        int Y;
        int ID;

        Drone(int X, int Y) {
            this.X = X;
            this.Y = Y;
        }

        double distance(Zone zone) {
            if (zone == null) {
                return Double.MAX_VALUE;
            }
            return Math.hypot(zone.X - X, zone.Y - Y);
        }

        void move(Zone[] zones, int[] teams) {

            int Z = zones.length;
            Zone emptyZone = zones[0];
            Zone controlled = zones[0];

            for (int k = 0; k < Z; k++) {
                if (teams[k] == -1) {
                    //if (distance(zones[k]) <= distance(emptyZone)) {
                    emptyZone = zones[k];
                    //}
                    break;
                } else {
                    if (distance(zones[k]) <= distance(controlled)) {
                        controlled = zones[k];
                    }
                }
            }

            if (distance(controlled) < distance(emptyZone)) {
                System.out.println(controlled);
            } else {
                System.out.println(emptyZone);
            }
        }
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int P = in.nextInt(); // number of players in the game (2 to 4 players)
        int ID = in.nextInt(); // ID of your player (0, 1, 2, or 3)
        int D = in.nextInt(); // number of drones in each team (3 to 11)
        int Z = in.nextInt(); // number of zones on the map (4 to 8)

        Zone[] zones = new Zone[Z];

        for (int i = 0; i < Z; i++) {
            int X = in.nextInt(); // corresponds to the position of the center of a zone. A zone is a circle with a radius of 100 units.
            int Y = in.nextInt();
            zones[i] = new Zone(X, Y);
        }

        // game loop
        while (true) {
            int controller[] = new int[Z];
            for (int i = 0; i < Z; i++) {
                int TID = in.nextInt(); // ID of the team controlling the zone (0, 1, 2, or 3) or -1 if it is not controlled. The zones are given in the same order as in the initialization.
                controller[i] = TID;
            }
            Drone[] drones = new Drone[P * D];
            for (int i = 0; i < P; i++) {
                for (int j = 0; j < D; j++) {
                    int DX = in.nextInt(); // The first D lines contain the coordinates of drones of a player with the ID 0, the following D lines those of the drones of player 1, and thus it continues until the last player.
                    int DY = in.nextInt();
                    drones[i * D + j] = new Drone(DX, DY);
                    drones[i * D + j].ID = i;
                }
            }
            for (int i = 0; i < D; i++) {

                // Write an action using System.out.println()
                // To debug: System.err.println("Debug messages...");
                drones[ID * D + i].move(zones, controller);

                //System.out.println("20 20"); // output a destination point to be reached by one of your drones. The first line corresponds to the first of your drones that you were provided as input, the next to the second, etc.
            }
        }
    }
}
